﻿using PokemonRemake.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonRemake
{
    public static class TypeData
    {
        public static Type Normal = new Type("Normal", Resources.typeNORMAL);
        public static Type Feu = new Type("Feu", Resources.typeFIRE);
        public static Type Eau = new Type("Eau", Resources.typeWATER);
        public static Type Eletrik = new Type("Eletrik", Resources.typeELECTRIC);
        public static Type Plante = new Type("Plante", Resources.typeGRASS);
        public static Type Spectre = new Type("Spectre", Resources.typeGHOST);
        public static Type Sol = new Type("Sol", Resources.typeGROUND);
        public static Type Dragon = new Type("Dragon", Resources.typeDRAGON);
        public static Type Glace = new Type("Glace", Resources.typeICE);
        public static Type Combat = new Type("Combat", Resources.typeFIGHTING);
        public static Type Poison = new Type("Poison", Resources.typePOISON);
        public static Type Psy = new Type("Psy", Resources.typePSYCHIC);
        public static Type Vol = new Type("Vol", Resources.typeFLYING);
        public static Type Insect = new Type("Insect", Resources.typeBUG);
        public static Type Roche = new Type("Roche", Resources.typeROCK);
        public static Type Tenebre = new Type("Ténèbre", Resources.typeDARK);
        public static Type Acier = new Type("Acier", Resources.typeSTEEL);
        public static Type Fee = new Type("Fée", Resources.typeFAIRY);

        public static void SetForcesFaiblesses()
        {
            // Type normal
            Normal.Faiblesses.Add(Spectre);
            Normal.Faiblesses.Add(Roche);
            Normal.Faiblesses.Add(Acier);

            // Type feu
            Feu.Faiblesses.Add(Eau);
            Feu.Faiblesses.Add(Feu);
            Feu.Faiblesses.Add(Roche);
            Feu.Faiblesses.Add(Dragon);
            Feu.Forces.Add(Plante);
            Feu.Forces.Add(Glace);
            Feu.Forces.Add(Insect);
            Feu.Forces.Add(Acier);

            // Type eau
            Eau.Faiblesses.Add(Eau);
            Eau.Faiblesses.Add(Plante);
            Eau.Faiblesses.Add(Dragon);
            Eau.Forces.Add(Feu);
            Eau.Forces.Add(Sol);
            Eau.Forces.Add(Roche);

            // Type plante
            Plante.Faiblesses.Add(Feu);
            Plante.Faiblesses.Add(Plante);
            Plante.Faiblesses.Add(Dragon);
            Plante.Faiblesses.Add(Poison);
            Plante.Faiblesses.Add(Vol);
            Plante.Faiblesses.Add(Insect);
            Plante.Faiblesses.Add(Acier);
            Plante.Forces.Add(Eau);
            Plante.Forces.Add(Sol);
            Plante.Forces.Add(Roche);

            // Type electrik
            Eletrik.Faiblesses.Add(Plante);
            Eletrik.Faiblesses.Add(Eletrik);
            Eletrik.Faiblesses.Add(Dragon);
            Eletrik.Faiblesses.Add(Sol);
            Eletrik.Forces.Add(Eau);
            Eletrik.Forces.Add(Vol);
        }
    }
}
