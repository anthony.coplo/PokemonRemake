﻿using System.Collections.Generic;

namespace PokemonRemake
{
    /// <summary>
    /// Initialisation du jeu
    /// </summary>
    public static class Initialisation
    {
        /// <summary>
        /// Initialisation des pokémons
        /// </summary>
        public static void Init()
        {
            TypeData.SetForcesFaiblesses();
            PokemonData.Pokemons = new List<Pokemon>()
            {
                PokemonData.Pikachu,
                PokemonData.Grolem,
                PokemonData.Bulbasaur,
                PokemonData.Electabuzz,
                PokemonData.Gengar,
                PokemonData.Gyarados,
                PokemonData.Jolteon,
                PokemonData.Mewtwo,
                PokemonData.Poliwhirl,
                PokemonData.Carapuce,
                PokemonData.Insecateur
            };
        }

    }
}
