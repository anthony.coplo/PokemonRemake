﻿using PokemonRemake.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonRemake
{
    public static class PokemonData
    {
        public static List<Pokemon> Pokemons;

        public static Pokemon Pikachu = new Pokemon("Pikachu",
            249,
            new Attaque[] {
                AttaqueData.Charge,
                AttaqueData.FatalFoudre,
                AttaqueData.Tonnerre,
                AttaqueData.ViveAttaque
            },
            TypeData.Eletrik, 
            null, 
            Resources.pikachud, 
            Resources.pikachu);

        public static Pokemon Grolem = new Pokemon("Grolem",
            249,
            new Attaque[] {
                        AttaqueData.Charge,
                        AttaqueData.Eboulement,
                        AttaqueData.Avalanche,
                        AttaqueData.ViveAttaque
            },
            TypeData.Sol,
            TypeData.Roche,
            Resources.golemd,
            Resources.golem);

        public static Pokemon Bulbasaur = new Pokemon("Bulbizzare",
            249,
            new Attaque[] {
                        AttaqueData.Charge,
                        AttaqueData.FouetLianes ,
                        AttaqueData.Trancherbe,
                        AttaqueData.CanonGraine
            },
            TypeData.Sol,
            TypeData.Plante,
            Resources.bulbasaurd,
            Resources.bulbasaur);


        public static Pokemon Electabuzz = new Pokemon("Electabuzz",
            249,
            new Attaque[] {
                        AttaqueData.Balayage,
                        AttaqueData.Point_Eclair ,
                        AttaqueData.Tonnerre,
                        AttaqueData.FatalFoudre
            },
            TypeData.Sol,
            TypeData.Eletrik,
            Resources.electabuzzd,
            Resources.electabuzz);

        public static Pokemon Gengar = new Pokemon("Ectoplasma",
            249,
            new Attaque[] {
                        AttaqueData.Hypnose,
                        AttaqueData.Malediction ,
                        AttaqueData.Vibrobscure,
                        AttaqueData.Cauchemar
            },
            TypeData.Poison,
            TypeData.Spectre,
            Resources.gengard,
            Resources.gengar);

        public static Pokemon Gyarados = new Pokemon("Leviator",
            249,
            new Attaque[] {
                        AttaqueData.Hydroqueue,
                        AttaqueData.Machouille ,
                        AttaqueData.Draco_rage,
                        AttaqueData.Laser_Glace
            },
            TypeData.Eau,
            TypeData.Dragon,
            Resources.gyaradosd,
            Resources.gyarados);


        public static Pokemon Jolteon = new Pokemon("Voltali",
            249,
            new Attaque[] {
                        AttaqueData.Charge,
                        AttaqueData.ViveAttaque ,
                        AttaqueData.Croc_eclair,
                        AttaqueData.FatalFoudre
            },
            TypeData.Eletrik,
            TypeData.Normal,
            Resources.jolteond,
            Resources.jolteon);

        public static Pokemon Mewtwo = new Pokemon("Mewtwo",
            249,
            new Attaque[] {
                        AttaqueData.Choc_mental,
                        AttaqueData.Psyko ,
                        AttaqueData.Brume,
                        AttaqueData.Frappe_psy
            },
            TypeData.Psy,
            TypeData.Glace,
            Resources.mewtwod,
            Resources.mewtwo);

        public static Pokemon Gyrados = new Pokemon("Léviator",
            249,
            new Attaque[] {
                        AttaqueData.Draco_rage,
                        AttaqueData.Croc_givre,
                        AttaqueData.Hydroqueue,
                        AttaqueData.Danse_pluie
            },
            TypeData.Eau,
            TypeData.Glace,
            Resources.gyaradosd,
            Resources.gyarados);


        public static Pokemon Poliwhirl = new Pokemon("Têtarte",
            249,
            new Attaque[] {
                        AttaqueData.Danse_pluie,
                        AttaqueData.Hypnose,
                        AttaqueData.Hydrocanon,
                        AttaqueData.Vibraqua
            },
            TypeData.Eau,
            TypeData.Psy,
            Resources.poliwhirld,
            Resources.poliwhirl);

    }
}
