﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonRemake
{
    public static class AttaqueData
    {

        /*                                        +===================+                     
                                                  |TOUTES LES ATTAQUES|  
                                                  +===================+
        */


        
        // type NORMAL
        public static Attaque Charge = new Attaque("Charge", 40, TypeData.Normal);
        public static Attaque ViveAttaque = new Attaque("Vive-Attaque", 40, TypeData.Normal);
        public static Attaque Machouille = new Attaque("Machouille", 50, TypeData.Normal);

        // type ELECTRIQUE
        public static Attaque Tonnerre = new Attaque("Tonnerre", 90, TypeData.Eletrik);
        public static Attaque FatalFoudre = new Attaque("Fatal-Foudre", 50, TypeData.Eletrik);
        public static Attaque Croc_eclair = new Attaque("Croc_eclair", 60, TypeData.Eletrik);

        // type ROCHE
        public static Attaque Avalanche = new Attaque("Avalanche", 50, TypeData.Roche);

        // type SOL
        public static Attaque Balayage = new Attaque("Balayage", 60, TypeData.Sol);
        public static Attaque Point_Eclair = new Attaque("Point-Eclair", 70, TypeData.Sol);
        public static Attaque Eboulement = new Attaque("Eboulement", 60, TypeData.Sol);

        // type PLANTE
        public static Attaque FouetLianes = new Attaque("Fouet lianes", 40, TypeData.Plante);
        public static Attaque Trancherbe = new Attaque("Fouet lianes", 60, TypeData.Plante);
        public static Attaque CanonGraine = new Attaque("Fouet lianes", 90, TypeData.Plante);

        // type POISON
        public static Attaque Hypnose = new Attaque("Hypnose", 50, TypeData.Poison);

        // type SPECTRE
        public static Attaque Malediction = new Attaque("Malediction", 40, TypeData.Spectre);
        public static Attaque Vibrobscure = new Attaque("Vibrobscure", 50, TypeData.Spectre);
        public static Attaque Cauchemar = new Attaque("Vibrobscure", 70, TypeData.Spectre);

        // type DRAGON
        public static Attaque Draco_rage = new Attaque("Draco_rage", 70, TypeData.Dragon);

        // type PSY
        public static Attaque Choc_mental = new Attaque("Choc_mental", 60, TypeData.Psy);
        public static Attaque Psyko = new Attaque("Psyko", 90, TypeData.Psy);
        public static Attaque Frappe_psy = new Attaque("Frappe_psy", 90, TypeData.Psy);

        // type GLACE
        public static Attaque Laser_Glace = new Attaque("Laser_Glace", 70, TypeData.Glace);
        public static Attaque Brume = new Attaque("Brume", 60, TypeData.Glace);
        public static Attaque Croc_givre = new Attaque("Croc_givre", 60, TypeData.Glace);

        // type EAU 
        public static Attaque Hydroqueue = new Attaque("Hydroqueue", 70, TypeData.Eau);
        public static Attaque Danse_pluie = new Attaque("Danse_pluie", 80, TypeData.Eau);
        public static Attaque Hydrocanon = new Attaque("Hydrocanon", 60, TypeData.Eau);
        public static Attaque Vibraqua = new Attaque("Hydrocanon", 70, TypeData.Eau);

    }
}
