﻿namespace PokemonRemake
{
    /// <summary>
    /// Gestion des joueurs
    /// </summary>
    public class Joueur
    {
        public string Nom { get; set; }
        public Pokemon Pokemon { get; set; }

        /// <summary>
        /// Constructeur par défault
        /// </summary>
        public Joueur() : base() { }

        /// <summary>
        /// Création d'un joueurs
        /// </summary>
        /// <param name="nom">Nom du joueur</param>
        /// <param name="pokemon">Pokemon du joueur</param>
        public Joueur(string nom, Pokemon pokemon)
        {
            Nom = nom;
            Pokemon = pokemon;
        }

        /// <summary>
        /// Renvoi le nom du joueur
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Nom;
        }

        /// <summary>
        /// Renvoie vrai si le joueur correspond
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            bool temp = false;
            if (obj is string)
                temp = Nom == (string)obj;
            else if (obj is Joueur)
                temp = obj == this;
            return temp;
        }

        /// <summary>
        /// Renvoie hashcode
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }
}
