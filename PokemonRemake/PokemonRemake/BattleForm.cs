﻿using System;
using System.Media;
using System.Windows.Forms;

namespace PokemonRemake
{
    public partial class BattleForm : Form
    {
        private SoundPlayer soundPlayer;

        private Partie partie;

        public BattleForm(Partie partie)
        {
            InitializeComponent();
            this.partie = partie;
            soundPlayer = new SoundPlayer(Properties.Resources.combat);
            this.partie.SoundPlayer = soundPlayer;
            this.partie.BattleForm = this;
        }

        private void BattleForm_Load(object sender, EventArgs e)
        {
            // Images des pokémons
            picb_PokemonBas.Image = partie.Joueurs[0].Pokemon.ImageDos;
            picb_PokemonHaut.Image = partie.Joueurs[1].Pokemon.ImageFace;

            // Positionnement de l'image (haut/bas)
            picb_PokemonBas.Location = partie.Joueurs[0].Pokemon.Locations[0];
            picb_PokemonHaut.Location = partie.Joueurs[1].Pokemon.Locations[1];

            // Images des types du pokémons 1
            picb_Type1Pkm1.Image = partie.Joueurs[0].Pokemon.Type1.Image;
            if (partie.Joueurs[0].Pokemon.Type2 != null)
                picb_Type2Pkm1.Image = partie.Joueurs[0].Pokemon.Type2.Image;
            else
                picb_Type2Pkm1.Image = null;
            
            // Images des types pokémons 2
            picb_Type1Pkm2.Image = partie.Joueurs[1].Pokemon.Type1.Image;
            if (partie.Joueurs[1].Pokemon.Type2 != null)
                picb_Type2Pkm2.Image = partie.Joueurs[1].Pokemon.Type2.Image;
            else
                picb_Type2Pkm2.Image = null;

            // Progress bar pokémon vie
            pb_PokemonJ1.Maximum = partie.Joueurs[0].Pokemon.VieMax;
            pb_PokemonJ2.Maximum = partie.Joueurs[1].Pokemon.VieMax;

            // Label nom pokémon
            l_NomPokemon1.Text = partie.Joueurs[0].Pokemon.Nom;
            l_NomPokemon2.Text = partie.Joueurs[1].Pokemon.Nom;
            UpdateForm();

            soundPlayer.PlayLooping();

        }

        private void UpdateForm()
        {
            pb_PokemonJ1.Value = partie.Joueurs[0].Pokemon.Vie;
            pb_PokemonJ2.Value = partie.Joueurs[1].Pokemon.Vie;

            if (pb_PokemonJ1.Value < ((pb_PokemonJ1.Maximum * 50) / 100))
            {
                pb_PokemonJ1.BarColor = System.Drawing.Color.FromArgb(((System.Byte)(255)),
                     ((System.Byte)(153)), ((System.Byte)(51)));
            }

            if (pb_PokemonJ1.Value < ((pb_PokemonJ1.Maximum * 20) / 100))
            {
                pb_PokemonJ1.BarColor = System.Drawing.Color.FromArgb(((System.Byte)(255)),
                     ((System.Byte)(51)), ((System.Byte)(51)));
            }

            if (pb_PokemonJ2.Value < ((pb_PokemonJ2.Maximum * 50) / 100))
            {
                pb_PokemonJ2.BarColor = System.Drawing.Color.FromArgb(((System.Byte)(255)),
                     ((System.Byte)(153)), ((System.Byte)(51)));
            }

            if (pb_PokemonJ2.Value < ((pb_PokemonJ2.Maximum * 20) / 100))
            {
                pb_PokemonJ2.BarColor = System.Drawing.Color.FromArgb(((System.Byte)(255)),
                     ((System.Byte)(51)), ((System.Byte)(51)));
            }

            btn_Attaque1.Text = partie.JoueurActuel.Pokemon.Attaques[0].Nom + Environment.NewLine + " ";
            btn_Attaque2.Text = partie.JoueurActuel.Pokemon.Attaques[1].Nom + Environment.NewLine + " ";
            btn_Attaque3.Text = partie.JoueurActuel.Pokemon.Attaques[2].Nom + Environment.NewLine + " ";
            btn_Attaque4.Text = partie.JoueurActuel.Pokemon.Attaques[3].Nom + Environment.NewLine + " ";
            picb_Attaque1.Image = partie.JoueurActuel.Pokemon.Attaques[0].Type.Image;
            picb_Attaque2.Image = partie.JoueurActuel.Pokemon.Attaques[1].Type.Image;
            picb_Attaque3.Image = partie.JoueurActuel.Pokemon.Attaques[2].Type.Image;
            picb_Attaque4.Image = partie.JoueurActuel.Pokemon.Attaques[3].Type.Image;
        }

        private void btn_Attaque1_Click(object sender, EventArgs e)
        {
            partie.Attaque(0);
            StartAnim();
            UpdateForm();
        }

        private void btn_Attaque2_Click(object sender, EventArgs e)
        {
            partie.Attaque(1);
            StartAnim();
            UpdateForm();
        }

        private void btn_Attaque3_Click(object sender, EventArgs e)
        {
            partie.Attaque(2);
            StartAnim();
            UpdateForm();
        }

        private void btn_Attaque4_Click(object sender, EventArgs e)
        {
            partie.Attaque(3);
            StartAnim();
            UpdateForm();
        }

        private void StartAnim()
        {
            SetupButtons();
            AnimTimer.Start();
            WaitAnimTimer.Start();
        }

        private void SetupButtons()
        {
            foreach (Control ctrl in Controls)
            {
                if (ctrl is Button)
                {
                    Button btn = (Button)ctrl;
                    if (btn.Enabled)
                        btn.Enabled = false;
                    else
                        btn.Enabled = true;
                }
                if (ctrl is PictureBox)
                {
                    PictureBox picb = (PictureBox)ctrl;
                    if (picb.Name.Contains("Attaque")) {
                        if (picb.Enabled)
                            picb.Enabled = false;
                        else
                            picb.Enabled = true;
                    }
                }
            }
        }

        private void AnimTimer_Tick(object sender, EventArgs e)
        {
            if (partie.JoueurActuel == partie.Joueurs[0])
            {
                if (picb_PokemonBas.Image != null)
                    picb_PokemonBas.Image = null;
                else
                    picb_PokemonBas.Image = partie.Joueurs[0].Pokemon.ImageDos;
            }
            else
            {
                if (picb_PokemonHaut.Image != null)
                    picb_PokemonHaut.Image = null;
                else
                    picb_PokemonHaut.Image = partie.Joueurs[1].Pokemon.ImageFace;
            }
        }

        private void WaitAnimTimer_Tick(object sender, EventArgs e)
        {
            AnimTimer.Stop();
            if (partie.JoueurActuel == partie.Joueurs[0])
            {
                picb_PokemonBas.Image = partie.Joueurs[0].Pokemon.ImageDos;
            }
            else
            {
                picb_PokemonHaut.Image = partie.Joueurs[1].Pokemon.ImageFace;
            }
            WaitAnimTimer.Stop();
            SetupButtons();
        }

        private void BattleForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }

        private void picb_Attaque1_Click(object sender, EventArgs e)
        {
            partie.Attaque(0);
            StartAnim();
            UpdateForm();
        }

        private void picb_Attaque2_Click(object sender, EventArgs e)
        {
            partie.Attaque(1);
            StartAnim();
            UpdateForm();
        }

        private void picb_Attaque4_Click(object sender, EventArgs e)
        {
            partie.Attaque(3);
            StartAnim();
            UpdateForm();
        }

        private void picb_Attaque3_Click(object sender, EventArgs e)
        {
            partie.Attaque(2);
            StartAnim();
            UpdateForm();
        }
    }
}
