﻿using System.Collections.Generic;
using System.Drawing;

namespace PokemonRemake
{
    /// <summary>
    /// Gestion des types
    /// </summary>
    public class Type
    {
        public string Nom { get; set; }
        public Image Image { get; set; }

        public List<Type> Forces { get; set; }
        public List<Type> Faiblesses { get; set; }

        /// <summary>
        /// Constructeur par défault
        /// </summary>
        public Type() : base() { }


        /// <summary>
        /// Création d'un type
        /// </summary>
        /// <param name="nom">Nom du type</param>
        /// <param name="image">Image du type</param>
        public Type(string nom, Image image)
        {
            Nom = nom;
            Image = image;
            Faiblesses = new List<Type>();
            Forces = new List<Type>();
        }

        /// <summary>
        /// Renvoie le nom du type
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Nom;
        }

        /// <summary>
        /// Renvoie vrai si le joueur correspond
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            bool temp = false;
            if (obj is string)
                temp = Nom == (string)obj;
            else if (obj is Type)
                temp = obj == this;
            return temp;
        }

        /// <summary>
        /// Renvoie hashcode
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
