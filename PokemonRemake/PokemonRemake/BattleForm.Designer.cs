﻿namespace PokemonRemake
{
    partial class BattleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.picb_PokemonHaut = new System.Windows.Forms.PictureBox();
            this.picb_PokemonBas = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pb_PokemonJ1 = new ColorProgressBar.ColorProgressBar();
            this.pb_PokemonJ2 = new ColorProgressBar.ColorProgressBar();
            this.btn_Attaque1 = new System.Windows.Forms.Button();
            this.btn_Attaque2 = new System.Windows.Forms.Button();
            this.btn_Attaque3 = new System.Windows.Forms.Button();
            this.btn_Attaque4 = new System.Windows.Forms.Button();
            this.l_NomPokemon1 = new System.Windows.Forms.Label();
            this.l_NomPokemon2 = new System.Windows.Forms.Label();
            this.picb_Type2Pkm1 = new System.Windows.Forms.PictureBox();
            this.picb_Type1Pkm1 = new System.Windows.Forms.PictureBox();
            this.picb_Type1Pkm2 = new System.Windows.Forms.PictureBox();
            this.picb_Type2Pkm2 = new System.Windows.Forms.PictureBox();
            this.picb_Attaque1 = new System.Windows.Forms.PictureBox();
            this.picb_Attaque2 = new System.Windows.Forms.PictureBox();
            this.picb_Attaque3 = new System.Windows.Forms.PictureBox();
            this.picb_Attaque4 = new System.Windows.Forms.PictureBox();
            this.AnimTimer = new System.Windows.Forms.Timer(this.components);
            this.WaitAnimTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picb_PokemonHaut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_PokemonBas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_Type2Pkm1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_Type1Pkm1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_Type1Pkm2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_Type2Pkm2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_Attaque1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_Attaque2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_Attaque3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_Attaque4)).BeginInit();
            this.SuspendLayout();
            // 
            // picb_PokemonHaut
            // 
            this.picb_PokemonHaut.BackColor = System.Drawing.Color.Transparent;
            this.picb_PokemonHaut.Image = global::PokemonRemake.Properties.Resources.squirtle;
            this.picb_PokemonHaut.Location = new System.Drawing.Point(285, 101);
            this.picb_PokemonHaut.Name = "picb_PokemonHaut";
            this.picb_PokemonHaut.Size = new System.Drawing.Size(39, 43);
            this.picb_PokemonHaut.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picb_PokemonHaut.TabIndex = 0;
            this.picb_PokemonHaut.TabStop = false;
            // 
            // picb_PokemonBas
            // 
            this.picb_PokemonBas.BackColor = System.Drawing.Color.Transparent;
            this.picb_PokemonBas.Image = global::PokemonRemake.Properties.Resources.squirtled;
            this.picb_PokemonBas.Location = new System.Drawing.Point(82, 184);
            this.picb_PokemonBas.Name = "picb_PokemonBas";
            this.picb_PokemonBas.Size = new System.Drawing.Size(40, 45);
            this.picb_PokemonBas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picb_PokemonBas.TabIndex = 1;
            this.picb_PokemonBas.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox3.Image = global::PokemonRemake.Properties.Resources.barre;
            this.pictureBox3.Location = new System.Drawing.Point(246, 39);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(112, 25);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Image = global::PokemonRemake.Properties.Resources.barre;
            this.pictureBox4.Location = new System.Drawing.Point(51, 119);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(112, 25);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::PokemonRemake.Properties.Resources.logPNL;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(-38, 235);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(471, 167);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // pb_PokemonJ1
            // 
            this.pb_PokemonJ1.BarColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.pb_PokemonJ1.BorderColor = System.Drawing.Color.Black;
            this.pb_PokemonJ1.FillStyle = ColorProgressBar.ColorProgressBar.FillStyles.Solid;
            this.pb_PokemonJ1.Location = new System.Drawing.Point(92, 128);
            this.pb_PokemonJ1.Maximum = 100;
            this.pb_PokemonJ1.Minimum = 0;
            this.pb_PokemonJ1.Name = "pb_PokemonJ1";
            this.pb_PokemonJ1.Size = new System.Drawing.Size(50, 3);
            this.pb_PokemonJ1.Step = 10;
            this.pb_PokemonJ1.TabIndex = 5;
            this.pb_PokemonJ1.Value = 0;
            // 
            // pb_PokemonJ2
            // 
            this.pb_PokemonJ2.BarColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(255)))), ((int)(((byte)(153)))));
            this.pb_PokemonJ2.BorderColor = System.Drawing.Color.Black;
            this.pb_PokemonJ2.FillStyle = ColorProgressBar.ColorProgressBar.FillStyles.Solid;
            this.pb_PokemonJ2.Location = new System.Drawing.Point(287, 48);
            this.pb_PokemonJ2.Maximum = 100;
            this.pb_PokemonJ2.Minimum = 0;
            this.pb_PokemonJ2.Name = "pb_PokemonJ2";
            this.pb_PokemonJ2.Size = new System.Drawing.Size(50, 3);
            this.pb_PokemonJ2.Step = 10;
            this.pb_PokemonJ2.TabIndex = 6;
            this.pb_PokemonJ2.Value = 0;
            // 
            // btn_Attaque1
            // 
            this.btn_Attaque1.BackColor = System.Drawing.Color.Transparent;
            this.btn_Attaque1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Attaque1.Location = new System.Drawing.Point(49, 267);
            this.btn_Attaque1.Name = "btn_Attaque1";
            this.btn_Attaque1.Size = new System.Drawing.Size(114, 48);
            this.btn_Attaque1.TabIndex = 7;
            this.btn_Attaque1.Text = "btn_Attaque1\r\n ";
            this.btn_Attaque1.UseVisualStyleBackColor = false;
            this.btn_Attaque1.Click += new System.EventHandler(this.btn_Attaque1_Click);
            // 
            // btn_Attaque2
            // 
            this.btn_Attaque2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Attaque2.Location = new System.Drawing.Point(49, 321);
            this.btn_Attaque2.Name = "btn_Attaque2";
            this.btn_Attaque2.Size = new System.Drawing.Size(114, 48);
            this.btn_Attaque2.TabIndex = 8;
            this.btn_Attaque2.Text = "btn_Attaque2\r\n ";
            this.btn_Attaque2.UseVisualStyleBackColor = true;
            this.btn_Attaque2.Click += new System.EventHandler(this.btn_Attaque2_Click);
            // 
            // btn_Attaque3
            // 
            this.btn_Attaque3.BackColor = System.Drawing.Color.Transparent;
            this.btn_Attaque3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Attaque3.Location = new System.Drawing.Point(246, 321);
            this.btn_Attaque3.Name = "btn_Attaque3";
            this.btn_Attaque3.Size = new System.Drawing.Size(114, 48);
            this.btn_Attaque3.TabIndex = 9;
            this.btn_Attaque3.Text = "btn_Attaque3\r\n ";
            this.btn_Attaque3.UseVisualStyleBackColor = false;
            this.btn_Attaque3.Click += new System.EventHandler(this.btn_Attaque3_Click);
            // 
            // btn_Attaque4
            // 
            this.btn_Attaque4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Attaque4.Location = new System.Drawing.Point(246, 267);
            this.btn_Attaque4.Name = "btn_Attaque4";
            this.btn_Attaque4.Size = new System.Drawing.Size(114, 48);
            this.btn_Attaque4.TabIndex = 10;
            this.btn_Attaque4.Text = "btn_Attaque4\r\n ";
            this.btn_Attaque4.UseVisualStyleBackColor = true;
            this.btn_Attaque4.Click += new System.EventHandler(this.btn_Attaque4_Click);
            // 
            // l_NomPokemon1
            // 
            this.l_NomPokemon1.AutoSize = true;
            this.l_NomPokemon1.BackColor = System.Drawing.Color.Transparent;
            this.l_NomPokemon1.Location = new System.Drawing.Point(48, 112);
            this.l_NomPokemon1.Name = "l_NomPokemon1";
            this.l_NomPokemon1.Size = new System.Drawing.Size(46, 13);
            this.l_NomPokemon1.TabIndex = 11;
            this.l_NomPokemon1.Text = "Pikachu";
            // 
            // l_NomPokemon2
            // 
            this.l_NomPokemon2.AutoSize = true;
            this.l_NomPokemon2.BackColor = System.Drawing.Color.Transparent;
            this.l_NomPokemon2.Location = new System.Drawing.Point(243, 32);
            this.l_NomPokemon2.Name = "l_NomPokemon2";
            this.l_NomPokemon2.Size = new System.Drawing.Size(46, 13);
            this.l_NomPokemon2.TabIndex = 12;
            this.l_NomPokemon2.Text = "Pikachu";
            // 
            // picb_Type2Pkm1
            // 
            this.picb_Type2Pkm1.BackColor = System.Drawing.Color.Transparent;
            this.picb_Type2Pkm1.Location = new System.Drawing.Point(90, 139);
            this.picb_Type2Pkm1.Name = "picb_Type2Pkm1";
            this.picb_Type2Pkm1.Size = new System.Drawing.Size(32, 14);
            this.picb_Type2Pkm1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picb_Type2Pkm1.TabIndex = 13;
            this.picb_Type2Pkm1.TabStop = false;
            // 
            // picb_Type1Pkm1
            // 
            this.picb_Type1Pkm1.BackColor = System.Drawing.Color.Transparent;
            this.picb_Type1Pkm1.Location = new System.Drawing.Point(126, 139);
            this.picb_Type1Pkm1.Name = "picb_Type1Pkm1";
            this.picb_Type1Pkm1.Size = new System.Drawing.Size(32, 14);
            this.picb_Type1Pkm1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picb_Type1Pkm1.TabIndex = 14;
            this.picb_Type1Pkm1.TabStop = false;
            // 
            // picb_Type1Pkm2
            // 
            this.picb_Type1Pkm2.BackColor = System.Drawing.Color.Transparent;
            this.picb_Type1Pkm2.Location = new System.Drawing.Point(321, 60);
            this.picb_Type1Pkm2.Name = "picb_Type1Pkm2";
            this.picb_Type1Pkm2.Size = new System.Drawing.Size(32, 14);
            this.picb_Type1Pkm2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picb_Type1Pkm2.TabIndex = 15;
            this.picb_Type1Pkm2.TabStop = false;
            // 
            // picb_Type2Pkm2
            // 
            this.picb_Type2Pkm2.BackColor = System.Drawing.Color.Transparent;
            this.picb_Type2Pkm2.Location = new System.Drawing.Point(285, 60);
            this.picb_Type2Pkm2.Name = "picb_Type2Pkm2";
            this.picb_Type2Pkm2.Size = new System.Drawing.Size(32, 14);
            this.picb_Type2Pkm2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picb_Type2Pkm2.TabIndex = 16;
            this.picb_Type2Pkm2.TabStop = false;
            // 
            // picb_Attaque1
            // 
            this.picb_Attaque1.Location = new System.Drawing.Point(89, 296);
            this.picb_Attaque1.Name = "picb_Attaque1";
            this.picb_Attaque1.Size = new System.Drawing.Size(32, 14);
            this.picb_Attaque1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picb_Attaque1.TabIndex = 17;
            this.picb_Attaque1.TabStop = false;
            this.picb_Attaque1.Click += new System.EventHandler(this.picb_Attaque1_Click);
            // 
            // picb_Attaque2
            // 
            this.picb_Attaque2.Location = new System.Drawing.Point(89, 349);
            this.picb_Attaque2.Name = "picb_Attaque2";
            this.picb_Attaque2.Size = new System.Drawing.Size(32, 14);
            this.picb_Attaque2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picb_Attaque2.TabIndex = 18;
            this.picb_Attaque2.TabStop = false;
            this.picb_Attaque2.Click += new System.EventHandler(this.picb_Attaque2_Click);
            // 
            // picb_Attaque3
            // 
            this.picb_Attaque3.Location = new System.Drawing.Point(286, 349);
            this.picb_Attaque3.Name = "picb_Attaque3";
            this.picb_Attaque3.Size = new System.Drawing.Size(32, 14);
            this.picb_Attaque3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picb_Attaque3.TabIndex = 19;
            this.picb_Attaque3.TabStop = false;
            this.picb_Attaque3.Click += new System.EventHandler(this.picb_Attaque3_Click);
            // 
            // picb_Attaque4
            // 
            this.picb_Attaque4.Location = new System.Drawing.Point(286, 296);
            this.picb_Attaque4.Name = "picb_Attaque4";
            this.picb_Attaque4.Size = new System.Drawing.Size(32, 14);
            this.picb_Attaque4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picb_Attaque4.TabIndex = 20;
            this.picb_Attaque4.TabStop = false;
            this.picb_Attaque4.Click += new System.EventHandler(this.picb_Attaque4_Click);
            // 
            // AnimTimer
            // 
            this.AnimTimer.Interval = 200;
            this.AnimTimer.Tick += new System.EventHandler(this.AnimTimer_Tick);
            // 
            // WaitAnimTimer
            // 
            this.WaitAnimTimer.Interval = 1800;
            this.WaitAnimTimer.Tick += new System.EventHandler(this.WaitAnimTimer_Tick);
            // 
            // BattleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PokemonRemake.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(400, 397);
            this.Controls.Add(this.picb_Attaque4);
            this.Controls.Add(this.picb_Attaque3);
            this.Controls.Add(this.picb_Attaque2);
            this.Controls.Add(this.picb_Attaque1);
            this.Controls.Add(this.picb_Type2Pkm2);
            this.Controls.Add(this.picb_Type1Pkm2);
            this.Controls.Add(this.picb_Type1Pkm1);
            this.Controls.Add(this.picb_Type2Pkm1);
            this.Controls.Add(this.l_NomPokemon2);
            this.Controls.Add(this.l_NomPokemon1);
            this.Controls.Add(this.btn_Attaque4);
            this.Controls.Add(this.btn_Attaque3);
            this.Controls.Add(this.btn_Attaque2);
            this.Controls.Add(this.btn_Attaque1);
            this.Controls.Add(this.pb_PokemonJ2);
            this.Controls.Add(this.pb_PokemonJ1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.picb_PokemonBas);
            this.Controls.Add(this.picb_PokemonHaut);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "BattleForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pokemon Battle";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BattleForm_FormClosing);
            this.Load += new System.EventHandler(this.BattleForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picb_PokemonHaut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_PokemonBas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_Type2Pkm1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_Type1Pkm1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_Type1Pkm2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_Type2Pkm2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_Attaque1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_Attaque2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_Attaque3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_Attaque4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picb_PokemonHaut;
        private System.Windows.Forms.PictureBox picb_PokemonBas;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private ColorProgressBar.ColorProgressBar pb_PokemonJ1;
        private ColorProgressBar.ColorProgressBar pb_PokemonJ2;
        private System.Windows.Forms.Button btn_Attaque1;
        private System.Windows.Forms.Button btn_Attaque2;
        private System.Windows.Forms.Button btn_Attaque3;
        private System.Windows.Forms.Button btn_Attaque4;
        private System.Windows.Forms.Label l_NomPokemon1;
        private System.Windows.Forms.Label l_NomPokemon2;
        private System.Windows.Forms.PictureBox picb_Type2Pkm1;
        private System.Windows.Forms.PictureBox picb_Type1Pkm1;
        private System.Windows.Forms.PictureBox picb_Type1Pkm2;
        private System.Windows.Forms.PictureBox picb_Type2Pkm2;
        private System.Windows.Forms.PictureBox picb_Attaque1;
        private System.Windows.Forms.PictureBox picb_Attaque2;
        private System.Windows.Forms.PictureBox picb_Attaque3;
        private System.Windows.Forms.PictureBox picb_Attaque4;
        private System.Windows.Forms.Timer AnimTimer;
        private System.Windows.Forms.Timer WaitAnimTimer;
    }
}