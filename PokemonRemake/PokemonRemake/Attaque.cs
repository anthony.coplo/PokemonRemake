﻿namespace PokemonRemake
{
    /// <summary>
    /// Gestion des attaques
    /// </summary>
    public class Attaque
    {
        public string Nom { get; set; }
        public int Degat { get; set; }
        public Type Type { get; set; }


        /// <summary>
        /// Constructeur par défault
        /// </summary>
        public Attaque() : base() { }


        /// <summary>
        /// Création d'une attaque
        /// </summary>
        /// <param name="nom">Nom de l'attaque</param>
        /// <param name="degat">Degat de l'attaque</param>
        /// <param name="type">Type de l'attaque</param>
        public Attaque(string nom, int degat, Type type)
        {
            Nom = nom;
            Degat = degat;
            Type = type;
        }


        /// <summary>
        /// Renvoie le nom de l'attaque
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Nom;
        }
    }
}
