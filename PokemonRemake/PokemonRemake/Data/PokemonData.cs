﻿using PokemonRemake.Properties;
using System.Collections.Generic;
using System.Drawing;

namespace PokemonRemake
{
    public static class PokemonData
    {
        public static List<Pokemon> Pokemons;

        public static Pokemon Pikachu = new Pokemon("Pikachu",
            249,
            new Attaque[] {
                AttaqueData.Charge,
                AttaqueData.FatalFoudre,
                AttaqueData.Tonnerre,
                AttaqueData.ViveAttaque
            },
            TypeData.Eletrik,
            null,
            Resources.pikachud,
            Resources.pikachu,
            new Point[] {
                new Point(81, 179),
                new Point(287, 98)
            });

        public static Pokemon Grolem = new Pokemon("Grolem",
            249,
            new Attaque[] {
                AttaqueData.Charge,
                AttaqueData.Eboulement,
                AttaqueData.Avalanche,
                AttaqueData.ViveAttaque
            },
            TypeData.Sol,
            TypeData.Roche,
            Resources.golemd,
            Resources.golem,
            new Point[] {
                new Point(73, 173),
                new Point(265, 92)
            });

        public static Pokemon Bulbasaur = new Pokemon("Bulbizzare",
            249,
            new Attaque[] {
                AttaqueData.Charge,
                AttaqueData.FouetLianes ,
                AttaqueData.Trancherbe,
                AttaqueData.CanonGraine
            },
            TypeData.Poison,
            TypeData.Plante,
            Resources.bulbasaurd,
            Resources.bulbasaur,
            new Point[] {
                new Point(88, 186),
                new Point(280, 107)
            });


        public static Pokemon Electabuzz = new Pokemon("Electabuzz",
            249,
            new Attaque[] {
                AttaqueData.Balayage,
                AttaqueData.PointEclair ,
                AttaqueData.Tonnerre,
                AttaqueData.FatalFoudre
            },
            TypeData.Sol,
            TypeData.Eletrik,
            Resources.electabuzzd,
            Resources.electabuzz,
            new Point[] {
                new Point(70, 165),
                new Point(265, 80)
            });

        public static Pokemon Gengar = new Pokemon("Ectoplasma",
            249,
            new Attaque[] {
                AttaqueData.Hypnose,
                AttaqueData.Malediction ,
                AttaqueData.Vibrobscure,
                AttaqueData.Cauchemar
            },
            TypeData.Poison,
            TypeData.Spectre,
            Resources.gengard,
            Resources.gengar,
            new Point[] {
                new Point(70, 165),
                new Point(265, 80)
            });

        public static Pokemon Gyarados = new Pokemon("Leviator",
            249,
            new Attaque[] {
                AttaqueData.Hydroqueue,
                AttaqueData.Machouille ,
                AttaqueData.DracoRage,
                AttaqueData.LaserGlace
            },
            TypeData.Eau,
            TypeData.Dragon,
            Resources.gyaradosd,
            Resources.gyarados,
            new Point[] {
                new Point(55, 153),
                new Point(263, 75)
            });


        public static Pokemon Jolteon = new Pokemon("Voltali",
            249,
            new Attaque[] {
                AttaqueData.Charge,
                AttaqueData.ViveAttaque ,
                AttaqueData.CrocEclair,
                AttaqueData.FatalFoudre
            },
            TypeData.Eletrik,
            TypeData.Normal,
            Resources.jolteond,
            Resources.jolteon,
            new Point[] {
                new Point(87, 175),
                new Point(279, 97)
            });

        public static Pokemon Mewtwo = new Pokemon("Mewtwo",
            249,
            new Attaque[] {
                AttaqueData.ChocMental,
                AttaqueData.Psyko ,
                AttaqueData.Brume,
                AttaqueData.FrappePsy
            },
            TypeData.Psy,
            TypeData.Glace,
            Resources.mewtwod,
            Resources.mewtwo,
            new Point[] {
                new Point(51, 159),
                new Point(268, 75)
            });

        public static Pokemon Poliwhirl = new Pokemon("Têtarte",
            249,
            new Attaque[] {
                AttaqueData.DansePluie,
                AttaqueData.Hypnose,
                AttaqueData.Hydrocanon,
                AttaqueData.Vibraqua
            },
            TypeData.Eau,
            TypeData.Poison,
            Resources.poliwhirld,
            Resources.poliwhirl,
            new Point[] {
                new Point(65, 170),
                new Point(267, 92)
            });

        public static Pokemon Insecateur = new Pokemon("Insécateur",
            249,
            new Attaque[] {
                AttaqueData.ViveAttaque,
                AttaqueData.LameAir,
                AttaqueData.Bourdon,
                AttaqueData.Taillade
            },
            TypeData.Insect,
            TypeData.Vol,
            Resources.scytherd,
            Resources.scyther,
            new Point[] {
                new Point(79, 162),
                new Point(273, 92)
            });

        public static Pokemon Carapuce = new Pokemon("Carapuce",
            249,
            new Attaque[]
            {
                AttaqueData.Charge,
                AttaqueData.Hydrocanon,
                AttaqueData.Hydroqueue,
                AttaqueData.Vibraqua
            },
            TypeData.Eau,
            null,
            Resources.squirtled,
            Resources.squirtle,
            new Point[] {
                new Point(82, 184),
                new Point(285, 100)
            });

    }
}
