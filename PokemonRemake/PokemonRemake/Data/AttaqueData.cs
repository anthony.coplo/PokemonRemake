﻿namespace PokemonRemake
{
    public static class AttaqueData
    {

        // type NORMAL
        public static Attaque Charge = new Attaque("Charge", 40, TypeData.Normal);
        public static Attaque ViveAttaque = new Attaque("Vive-Attaque", 40, TypeData.Normal);
        public static Attaque Machouille = new Attaque("Machouille", 50, TypeData.Normal);

        // type ELECTRIQUE
        public static Attaque Tonnerre = new Attaque("Tonnerre", 90, TypeData.Eletrik);
        public static Attaque FatalFoudre = new Attaque("Fatal-Foudre", 50, TypeData.Eletrik);
        public static Attaque CrocEclair = new Attaque("Croc eclair", 60, TypeData.Eletrik);

        // type ROCHE
        public static Attaque Avalanche = new Attaque("Avalanche", 50, TypeData.Roche);

        // type SOL
        public static Attaque Balayage = new Attaque("Balayage", 60, TypeData.Sol);
        public static Attaque PointEclair = new Attaque("Point-Eclair", 70, TypeData.Sol);
        public static Attaque Eboulement = new Attaque("Eboulement", 60, TypeData.Sol);

        // type PLANTE
        public static Attaque FouetLianes = new Attaque("Fouet lianes", 40, TypeData.Plante);
        public static Attaque Trancherbe = new Attaque("Tranch' herbe", 60, TypeData.Plante);
        public static Attaque CanonGraine = new Attaque("Canon graine", 60, TypeData.Plante);

        // type POISON
        public static Attaque Hypnose = new Attaque("Hypnose", 50, TypeData.Poison);

        // type SPECTRE
        public static Attaque Malediction = new Attaque("Malediction", 40, TypeData.Spectre);
        public static Attaque Vibrobscure = new Attaque("Vibrobscure", 50, TypeData.Spectre);
        public static Attaque Cauchemar = new Attaque("Vibrobscure", 60, TypeData.Spectre);

        // type DRAGON
        public static Attaque DracoRage = new Attaque("Draco rage", 60, TypeData.Dragon);

        // type PSY
        public static Attaque ChocMental = new Attaque("Choc mental", 60, TypeData.Psy);
        public static Attaque Psyko = new Attaque("Psyko", 40, TypeData.Psy);
        public static Attaque FrappePsy = new Attaque("Frappe psy", 50, TypeData.Psy);

        // type GLACE
        public static Attaque LaserGlace = new Attaque("Laser glace", 50, TypeData.Glace);
        public static Attaque Brume = new Attaque("Brume", 60, TypeData.Glace);
        public static Attaque CrocGivre = new Attaque("Croc givre", 66, TypeData.Glace);

        // type EAU 
        public static Attaque Hydroqueue = new Attaque("Hydroqueue", 40, TypeData.Eau);
        public static Attaque DansePluie = new Attaque("Danse pluie", 60, TypeData.Eau);
        public static Attaque Hydrocanon = new Attaque("Hydrocanon", 60, TypeData.Eau);
        public static Attaque Vibraqua = new Attaque("Hydrocanon", 50, TypeData.Eau);

        // type Insect
        public static Attaque Taillade = new Attaque("Taillade", 50, TypeData.Insect);
        public static Attaque Bourdon = new Attaque("Bourdon", 50, TypeData.Insect);

        // type Vol
        public static Attaque LameAir = new Attaque("Lame d'Air", 55, TypeData.Vol);


    }
}
