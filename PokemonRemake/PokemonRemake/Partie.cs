﻿using System;
using System.Media;

namespace PokemonRemake
{
    /// <summary>
    /// Gestion des parties
    /// </summary>
    public class Partie
    {
        public Joueur[] Joueurs { get; set; }
        public Joueur JoueurActuel { get; set; }
        public SoundPlayer SoundPlayer { get; set; }

        public BattleForm BattleForm { get; set; }

        /// <summary>
        /// Constructeur avec parametre
        /// </summary>
        /// <param name="pseudoJ1"></param>
        /// <param name="pkmJ1"></param>
        /// <param name="pseudoJ2"></param>
        /// <param name="pkmJ2"></param>
        public Partie(string pseudoJ1, Pokemon pkmJ1, string pseudoJ2, Pokemon pkmJ2)
        {
            Joueurs = new Joueur[] { new Joueur(pseudoJ1, pkmJ1), new Joueur(pseudoJ2, pkmJ2) };
            JoueurActuel = Joueurs[0];
        }

        /// <summary>
        /// S'active à chaque clique sur un button
        /// </summary>
        /// <param name="indexAttack"></param>
        public void Attaque(int indexAttack)
        {
            Joueur autreJoueur;
            Attaque attaque = JoueurActuel.Pokemon.Attaques[indexAttack];

            if (JoueurActuel == Joueurs[0])
                autreJoueur = Joueurs[1];
            else
                autreJoueur = Joueurs[0];

            int degatAttaque = AttaqueModifier(attaque, autreJoueur.Pokemon);

            if (autreJoueur.Pokemon.Vie > degatAttaque)
                autreJoueur.Pokemon.Vie = autreJoueur.Pokemon.Vie - degatAttaque;
            else
                autreJoueur.Pokemon.Vie = 0;

            if (JoueurActuel.Pokemon.Vie > 0 && autreJoueur.Pokemon.Vie > 0)
                JoueurActuel = autreJoueur;
            else
                FinDePartie();    
        }

        /// <summary>
        /// Modifie les dégats des attaques en fonction des forces et faiblesses
        /// </summary>
        /// <param name="attaque"></param>
        /// <param name="pokemonEnnemi"></param>
        /// <returns></returns>
        private int AttaqueModifier(Attaque attaque, Pokemon pokemonEnnemi)
        {
            int attaqueDmg = attaque.Degat;
            Type attaqueType = attaque.Type;
            Type pkmType1 = pokemonEnnemi.Type1;
            Type pkmType2 = pokemonEnnemi.Type2;

            bool attaqueModifier = false;

            if (pkmType1 != null && !attaqueModifier)
            {
                if (attaqueType.Forces.Contains(pkmType1))
                {
                    attaqueDmg = attaqueDmg + (int)Math.Round(attaqueDmg * 0.2);
                }

                if (attaqueType.Faiblesses.Contains(pkmType1))
                {
                    attaqueDmg = attaqueDmg - (attaqueDmg / 2);
                }
                attaqueModifier = true;
            }

            if (pkmType2 != null && !attaqueModifier)
            {
                if (attaqueType.Forces.Contains(pkmType2))
                {
                    attaqueDmg = attaqueDmg + (int)Math.Round(attaqueDmg * 0.2);
                }

                if (attaqueType.Faiblesses.Contains(pkmType2))
                {
                    attaqueDmg = attaqueDmg - (attaqueDmg / 2);
                }
            }

            return attaqueDmg;
        }

        private void FinDePartie()
        {
            Joueur vainqueur;
            if (Joueurs[0].Pokemon.Vie == 0)
                vainqueur = Joueurs[1];
            else
                vainqueur = Joueurs[0];
            Joueurs[0].Pokemon.Vie = Joueurs[0].Pokemon.VieMax;
            Joueurs[1].Pokemon.Vie = Joueurs[0].Pokemon.VieMax;
            SoundPlayer.Stop();
            new EndForm(vainqueur).Show();
            BattleForm.Hide();
        }
        
    }
}
