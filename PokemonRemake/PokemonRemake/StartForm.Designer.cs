﻿namespace PokemonRemake
{
    partial class StartForm
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.picb_Logo = new System.Windows.Forms.PictureBox();
            this.btn_NouvellePartie = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picb_Logo)).BeginInit();
            this.SuspendLayout();
            // 
            // picb_Logo
            // 
            this.picb_Logo.BackColor = System.Drawing.Color.Transparent;
            this.picb_Logo.Image = global::PokemonRemake.Properties.Resources.logo;
            this.picb_Logo.Location = new System.Drawing.Point(32, 27);
            this.picb_Logo.Name = "picb_Logo";
            this.picb_Logo.Size = new System.Drawing.Size(251, 110);
            this.picb_Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picb_Logo.TabIndex = 0;
            this.picb_Logo.TabStop = false;
            // 
            // btn_NouvellePartie
            // 
            this.btn_NouvellePartie.Location = new System.Drawing.Point(73, 204);
            this.btn_NouvellePartie.Name = "btn_NouvellePartie";
            this.btn_NouvellePartie.Size = new System.Drawing.Size(167, 52);
            this.btn_NouvellePartie.TabIndex = 1;
            this.btn_NouvellePartie.Text = "Nouvelle partie";
            this.btn_NouvellePartie.UseVisualStyleBackColor = true;
            this.btn_NouvellePartie.Click += new System.EventHandler(this.btn_NouvellePartie_Click);
            // 
            // StartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PokemonRemake.Properties.Resources.startbg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(515, 310);
            this.Controls.Add(this.btn_NouvellePartie);
            this.Controls.Add(this.picb_Logo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "StartForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pokemon Battle";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StartForm_FormClosing);
            this.Load += new System.EventHandler(this.StartForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picb_Logo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picb_Logo;
        private System.Windows.Forms.Button btn_NouvellePartie;
    }
}

