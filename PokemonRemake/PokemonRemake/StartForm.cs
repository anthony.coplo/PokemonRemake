﻿using System;
using System.Media;
using System.Windows.Forms;

namespace PokemonRemake
{
    public partial class StartForm : Form
    {
        private SoundPlayer soundPlayer;

        public StartForm()
        {
            InitializeComponent();
            soundPlayer = new SoundPlayer(Properties.Resources.intro);
        }

        private void btn_NouvellePartie_Click(object sender, EventArgs e)
        {
            soundPlayer.Stop();
            new PokemonSelectForm().Show();
            Hide();
        }

        private void StartForm_Load(object sender, EventArgs e)
        {
            soundPlayer.PlayLooping();
        }

        private void StartForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
