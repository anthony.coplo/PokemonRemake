﻿using System;
using System.Media;
using System.Windows.Forms;

namespace PokemonRemake
{
    public partial class PokemonSelectForm : Form
    {
        private SoundPlayer soundPlayer;
        public PokemonSelectForm()
        {
            InitializeComponent();
            soundPlayer = new SoundPlayer(Properties.Resources.choix);
        }

        /// <summary>
        /// Initialisation d'une nouvelle partie (Nom du joueur + pokémon)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Combattre_Click(object sender, EventArgs e)
        {
            soundPlayer.Stop();
            if (cb_PokemonJ1.SelectedItem != null && cb_PokemonJ2.SelectedItem != null)
            {
                if (cb_PokemonJ1.SelectedItem != cb_PokemonJ2.SelectedItem)
                {
                    new BattleForm(new Partie(
                        txt_NomJ1.Text,
                        (Pokemon)cb_PokemonJ1.SelectedItem,
                        txt_NomJ2.Text,
                        (Pokemon)cb_PokemonJ2.SelectedItem
                        )).Show();
                    Hide();
                }
            }
        }

        /// <summary>
        /// Ajout des pokémons dans les combobox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PokemonSelectForm_Load(object sender, EventArgs e)
        {
            foreach (Pokemon pkm in PokemonData.Pokemons)
            {
                cb_PokemonJ1.Items.Add(pkm);
                cb_PokemonJ2.Items.Add(pkm);

                soundPlayer.PlayLooping();
            }
        }

        private void PokemonSelectForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
