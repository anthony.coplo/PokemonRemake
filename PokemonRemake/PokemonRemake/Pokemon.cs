﻿using System.Drawing;

namespace PokemonRemake
{
    /// <summary>
    /// Gestion des pokémons
    /// </summary>
    public class Pokemon
    {
        public string Nom { get; set; }
        public int Vie { get; set; }
        public int VieMax { get; set; }
        public Attaque[] Attaques { get; set; }
        public Type Type1 { get; set; }
        public Type Type2 { get; set; }
        public Image ImageDos { get; set; }
        public Image ImageFace { get; set; }

        /// <summary>
        /// Locations[0] = Bas
        /// Locations[1] = Haut
        /// </summary>
        public Point[] Locations { get; set; }

        /// <summary>
        /// Constructeur par défault
        /// </summary>
        public Pokemon() : base() { }

        /// <summary>
        /// Création d'un pokémon
        /// </summary>
        /// <param name="nom">Nom du pokémon</param>
        /// <param name="viemax">Vie maximum du pokémon</param>
        /// <param name="attaques">Attaques du pokémon</param>
        /// <param name="type">Type du pokémon</param>
        /// <param name="generation">Génération du pokémon</param>
        /// <param name="image">Image du pokémon</param>
        public Pokemon(string nom, int viemax, Attaque[] attaques, Type type1, Type type2, Image imageDos, Image imageFace, Point[] locations)
        {
            Nom = nom;
            Vie = viemax;
            VieMax = viemax;
            Attaques = attaques;
            Type1 = type1;
            Type2 = type2;
            ImageDos = imageDos;
            ImageFace = imageFace;
            Locations = locations;
        }

        /// <summary>
        /// Renvoie le nom du pokémon
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Nom;
        }

        /// <summary>
        /// Renvoie vrai si le pokémon correspond
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            bool temp = false;
            if (obj is string)
                temp = Nom == (string)obj;
            else if (obj is Pokemon)
                temp = obj == this;
            return temp;
        }

        /// <summary>
        /// Renvoie hashcode
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
