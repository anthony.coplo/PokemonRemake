﻿namespace PokemonRemake
{
    partial class EndForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picb_PokemonVainqueur = new System.Windows.Forms.PictureBox();
            this.l_Vainqueur = new System.Windows.Forms.Label();
            this.btn1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picb_PokemonVainqueur)).BeginInit();
            this.SuspendLayout();
            // 
            // picb_PokemonVainqueur
            // 
            this.picb_PokemonVainqueur.BackColor = System.Drawing.Color.Transparent;
            this.picb_PokemonVainqueur.Image = global::PokemonRemake.Properties.Resources.bulbasaur;
            this.picb_PokemonVainqueur.Location = new System.Drawing.Point(125, 122);
            this.picb_PokemonVainqueur.Name = "picb_PokemonVainqueur";
            this.picb_PokemonVainqueur.Size = new System.Drawing.Size(37, 38);
            this.picb_PokemonVainqueur.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picb_PokemonVainqueur.TabIndex = 0;
            this.picb_PokemonVainqueur.TabStop = false;
            // 
            // l_Vainqueur
            // 
            this.l_Vainqueur.AutoSize = true;
            this.l_Vainqueur.BackColor = System.Drawing.Color.Transparent;
            this.l_Vainqueur.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_Vainqueur.Location = new System.Drawing.Point(96, 32);
            this.l_Vainqueur.Name = "l_Vainqueur";
            this.l_Vainqueur.Size = new System.Drawing.Size(98, 24);
            this.l_Vainqueur.TabIndex = 1;
            this.l_Vainqueur.Text = "Vainqueur";
            this.l_Vainqueur.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(100, 208);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(94, 28);
            this.btn1.TabIndex = 2;
            this.btn1.Text = "rejouer";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // EndForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PokemonRemake.Properties.Resources.selectbg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(302, 262);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.l_Vainqueur);
            this.Controls.Add(this.picb_PokemonVainqueur);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "EndForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pokemon Battle";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EndForm_FormClosing);
            this.Load += new System.EventHandler(this.EndForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picb_PokemonVainqueur)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picb_PokemonVainqueur;
        private System.Windows.Forms.Label l_Vainqueur;
        private System.Windows.Forms.Button btn1;
    }
}