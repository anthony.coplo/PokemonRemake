﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PokemonRemake
{
    public partial class EndForm : Form
    {
        private Joueur vainqueur;
        private SoundPlayer soundPlayer;

        public EndForm(Joueur vainqueur)
        {
            InitializeComponent();
            this.vainqueur = vainqueur;
            soundPlayer = new SoundPlayer(Properties.Resources.fin);
        }

        private void EndForm_Load(object sender, EventArgs e)
        {
            soundPlayer.PlayLooping();
            Utils.CenterPictureBox(picb_PokemonVainqueur, (Bitmap)vainqueur.Pokemon.ImageFace);
 
        }

       
        private void EndForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            soundPlayer.Stop();
           // Environment.Exit(0);
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            Close();
            new PokemonSelectForm().Show();
            soundPlayer = new SoundPlayer(Properties.Resources.choix);
            soundPlayer.Play();
            
        }
    }
}
