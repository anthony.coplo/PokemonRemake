﻿namespace PokemonRemake
{
    partial class PokemonSelectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cb_PokemonJ1 = new System.Windows.Forms.ComboBox();
            this.cb_PokemonJ2 = new System.Windows.Forms.ComboBox();
            this.txt_NomJ1 = new System.Windows.Forms.TextBox();
            this.txt_NomJ2 = new System.Windows.Forms.TextBox();
            this.btn_Combattre = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cb_PokemonJ1
            // 
            this.cb_PokemonJ1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_PokemonJ1.FormattingEnabled = true;
            this.cb_PokemonJ1.Location = new System.Drawing.Point(12, 38);
            this.cb_PokemonJ1.Name = "cb_PokemonJ1";
            this.cb_PokemonJ1.Size = new System.Drawing.Size(121, 21);
            this.cb_PokemonJ1.TabIndex = 0;
            // 
            // cb_PokemonJ2
            // 
            this.cb_PokemonJ2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_PokemonJ2.FormattingEnabled = true;
            this.cb_PokemonJ2.Location = new System.Drawing.Point(151, 38);
            this.cb_PokemonJ2.Name = "cb_PokemonJ2";
            this.cb_PokemonJ2.Size = new System.Drawing.Size(121, 21);
            this.cb_PokemonJ2.TabIndex = 1;
            // 
            // txt_NomJ1
            // 
            this.txt_NomJ1.Location = new System.Drawing.Point(12, 12);
            this.txt_NomJ1.Name = "txt_NomJ1";
            this.txt_NomJ1.Size = new System.Drawing.Size(121, 20);
            this.txt_NomJ1.TabIndex = 2;
            this.txt_NomJ1.Text = "Nom du joueur 1";
            // 
            // txt_NomJ2
            // 
            this.txt_NomJ2.Location = new System.Drawing.Point(151, 12);
            this.txt_NomJ2.Name = "txt_NomJ2";
            this.txt_NomJ2.Size = new System.Drawing.Size(121, 20);
            this.txt_NomJ2.TabIndex = 3;
            this.txt_NomJ2.Text = "Nom du joueur 2";
            // 
            // btn_Combattre
            // 
            this.btn_Combattre.BackColor = System.Drawing.Color.Transparent;
            this.btn_Combattre.Location = new System.Drawing.Point(90, 79);
            this.btn_Combattre.Name = "btn_Combattre";
            this.btn_Combattre.Size = new System.Drawing.Size(105, 36);
            this.btn_Combattre.TabIndex = 4;
            this.btn_Combattre.Text = "Combattre";
            this.btn_Combattre.UseVisualStyleBackColor = false;
            this.btn_Combattre.Click += new System.EventHandler(this.btn_Combattre_Click);
            // 
            // PokemonSelectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::PokemonRemake.Properties.Resources.selectbg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(287, 127);
            this.Controls.Add(this.btn_Combattre);
            this.Controls.Add(this.txt_NomJ2);
            this.Controls.Add(this.txt_NomJ1);
            this.Controls.Add(this.cb_PokemonJ2);
            this.Controls.Add(this.cb_PokemonJ1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "PokemonSelectForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pokemon Battle";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PokemonSelectForm_FormClosing);
            this.Load += new System.EventHandler(this.PokemonSelectForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cb_PokemonJ1;
        private System.Windows.Forms.ComboBox cb_PokemonJ2;
        private System.Windows.Forms.TextBox txt_NomJ1;
        private System.Windows.Forms.TextBox txt_NomJ2;
        private System.Windows.Forms.Button btn_Combattre;
    }
}